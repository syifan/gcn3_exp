package main

import (
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/noc"
)

//BuildNR9NanoTSMPlatform creates a platform that equips with several R9Nano
//GPUs
func BuildNR9NanoTSMPlatform(
	numGPUs int,
) (
	akita.Engine,
	*driver.Driver,
) {
	var engine akita.Engine

	engine = akita.NewSerialEngine()

	mmu := vm.NewMMU("MMU", engine, &vm.DefaultPageTableFactory{})
	mmu.Latency = 100
	mmu.ShootdownLatency = 50
	gpuDriver := driver.NewDriver(engine, mmu)
	//connection := akita.NewDirectConnection(engine)
	connection := noc.NewFixedBandwidthConnection(32, engine, 1*akita.GHz)
	connection.SrcBufferCapacity = 40960000

	memCtrls := createTSMMemoryControllers(engine, connection)

	gpuBuilder := R9NanoGPUBuilder{
		GPUName:           "GPU",
		Engine:            engine,
		Driver:            gpuDriver,
		MMU:               mmu,
		TSMMemControllers: memCtrls,
		ExternalConn:      connection,
	}

	for i := 1; i < numGPUs+1; i++ {
		gpuBuilder.GPUName = fmt.Sprintf("GPU_%d", i)
		gpuBuilder.GPUMemAddrOffset = uint64(i) * 4 * mem.GB
		gpu := gpuBuilder.Build()
		gpuDriver.RegisterGPU(gpu, 4*mem.GB)
		gpu.Driver = gpuDriver.ToGPUs
	}

	connection.PlugIn(gpuDriver.ToGPUs)
	connection.PlugIn(mmu.ToTop)

	return engine, gpuDriver
}

func createTSMMemoryControllers(
	engine akita.Engine,
	conn akita.Connection,
) []*mem.IdealMemController {
	numMemCtrl := 32
	memCtrls := make([]*mem.IdealMemController, 0)

	for i := 0; i < numMemCtrl; i++ {
		memCtrl := mem.NewIdealMemController(
			fmt.Sprintf("TSM.DRAM_%d", i),
			engine,
			32*mem.GB)
		memCtrls = append(memCtrls, memCtrl)

		addrConverter := mem.InterleavingConverter{
			InterleavingSize:    4096,
			TotalNumOfElements:  numMemCtrl,
			CurrentElementIndex: i,
			Offset:              4 * mem.GB,
		}
		memCtrl.AddressConverter = addrConverter

		conn.PlugIn(memCtrl.ToTop)
	}

	return memCtrls
}
