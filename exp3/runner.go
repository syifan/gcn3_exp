package main

import (
	"fmt"
	"sync"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/benchmarks"
	"gitlab.com/akita/gcn3/driver"
)

type Runner struct {
	Engine            akita.Engine
	GPUDriver         *driver.Driver
	KernelTimeCounter *driver.KernelTimeCounter
	Benchmarks        []benchmarks.Benchmark
	gpuIDs            []int
}

func (r *Runner) Init() {
	r.KernelTimeCounter = driver.NewKernelTimeCounter()
	r.Engine, r.GPUDriver = BuildNR9NanoTSMPlatform(4)
	r.GPUDriver.AcceptHook(r.KernelTimeCounter)
	r.GPUDriver.Run()
}

// AddBenchmark adds an benchmark that the driver runs
func (r *Runner) AddBenchmark(b benchmarks.Benchmark) {
	b.SelectGPU([]int{1, 2, 3, 4})
	r.Benchmarks = append(r.Benchmarks, b)
}

// Run runs the benchmark on the simulator
func (r *Runner) Run() {
	var wg sync.WaitGroup
	for _, b := range r.Benchmarks {
		wg.Add(1)
		go func(b benchmarks.Benchmark, wg *sync.WaitGroup) {
			b.Run()

			b.Verify()

			wg.Done()
		}(b, &wg)
	}
	wg.Wait()

	r.Engine.Finished()
	fmt.Printf("Kernel time: %.12f\n", r.KernelTimeCounter.TotalTime)
	fmt.Printf("Total time: %.12f\n", r.Engine.CurrentTime())
}
