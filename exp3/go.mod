module gitlab.com/syifan/gcn3_exp/exp3

require (
	gitlab.com/akita/akita v1.2.1
	gitlab.com/akita/gcn3 v1.3.1
	gitlab.com/akita/mem v1.1.2
	gitlab.com/akita/noc v1.1.2
)

go 1.12
