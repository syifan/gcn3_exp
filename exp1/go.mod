module gitlab.com/syifan/gcn3_exp/exp1

require (
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/fatih/motion v0.0.0-20180408211639-218875ebe238 // indirect
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/jstemmer/gotags v1.4.1 // indirect
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/klauspost/asmfmt v1.2.0 // indirect
	github.com/koron/iferr v0.0.0-20180615142939-bb332a3b1d91 // indirect
	github.com/nicksnyder/go-i18n v1.10.0 // indirect
	github.com/pelletier/go-toml v1.2.0 // indirect
	github.com/zmb3/gogetdoc v0.0.0-20190228002656-b37376c5da6a // indirect
	gitlab.com/akita/gcn3 v1.3.1
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c // indirect
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650 // indirect
	golang.org/x/sys v0.0.0-20190322080309-f49334f85ddc // indirect
	golang.org/x/tools v0.0.0-20190328030505-8f05a32dce9f // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
	honnef.co/go/tools v0.0.0-20190319011948-d116c56a00f3 // indirect
)

go 1.12
